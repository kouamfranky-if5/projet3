




export function getTagsPhotographe(tags) {
    let content;
    let filtre = document.querySelector(".filtre_tag_name");
    let elts = document.createElement("div");

    tags.forEach(element => {

        let elt = document.createElement("p");
        elt.classList.add("tag_name", "tag_" + element);
        elt.innerText = `#${element}`;
        // elt.onclick();
        // elt.addEventListener("click",filtreTag(element));
        //  elt = `<p class="tag_name">  </p>`
        // elt.add
        elts.appendChild(elt);
        // filtre.appendChild(elt);
    });

    return elts.outerHTML;
}

// permet de recuperer tout les tags des photographes
export function getAllTags(data) {
    let listAllTags = [];
    for (let index = 0; index < data.length; index++) {
        const item = data[index];
        item.tags.forEach(tag => {
            if (!listAllTags.includes(tag)) {
                listAllTags.push(tag);
            }
        });
    }
    return listAllTags;
}

export function contentArray(tags, selectionTags) {
    let response = []
    for (let index = 0; index < selectionTags.length; index++) {
        const tag = selectionTags[index];

        if (tags.includes(tag.split('#')[1])) {
            response.push(true);
        } else
            response.push(false);
    }

    return response.includes(false)? false : true
}