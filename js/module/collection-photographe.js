
import * as utils from './utils.js'





export function getCollectionPhotographe(data, content, name) {
    //    console.log("data",data);
    let medias = '';
    for (let index = 0; index < data.length; index++) {
        const item = data[index];
        medias += `
        <article class="card_detail">
            ${getImageOrVideo(name, item)}
            <section class="card_detail__title">
                <h3>${item.title}</h3>
                <span class="prix-favories">
                ${item.price} €
                <span id="heart_${item.id}" class> ${item.likes} <i class="far fa-heart"> </i> </span>
                </span>
            </section>
        </article>
        `;
    }

    content.innerHTML = medias;
    return content;
}

function getImageOrVideo(name, item) {
   
    let content='';
    let elts = '';
    if (item.image) {
        content = `
        <div class="card_detail__card_img">
            <a id="media_${item.id}" href="#${item.id}">
                <img src="assets/images/${name}/${item.image}"
                    alt="photo de ${item.title}"></img>
            </a>
        </div>
    `
    } else { 
        content = ` 
        <div class="card_detail__card_video">
           <a id="media_${item.id}" href="#${item.id}">
               <img class="play-video" src="assets/images/192px.svg" alt="play video">
               <video src="assets/images/${name}/${item.video}"
                   >  video de ${item.title} </video>
           </a>
       </div>
       `
    }
 
    elts += content 
    return elts;
}