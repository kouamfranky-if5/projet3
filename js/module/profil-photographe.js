import * as utils from './utils.js'



export function getProfifPhotographe(data,content) { 
   
    let photographes='';
    for (let index = 0; index < data.length; index++) {
        const item = data[index];
        photographes += `
        <article class="card_profil_photographe">
            <div class="card_img_profil" id="${item.id}">
                <a href="detail.html">
                    <img src="assets/images/Photographers_ID_Photos/${item.portrait}"
                        alt="photo de ${item.name}">
                </a>
            </div>
            <div class="card_profil_photographe__detail">
                <h3 class="card_profil_photographe__detail--titre">${item.name}</h3>
                <p class="card_profil_photographe__detail--localisation">${item.city} ${item.country}</p>
                <p class="card_profil_photographe__detail--description"> ${item.tagline} </p>
                <p class="card_profil_photographe__detail--prix"> ${item.price}€/jour </p>
                ${utils.getTagsPhotographe(item.tags)} 
            </div>
        </article>
        `;
    }
    
content.innerHTML = photographes;
return content;
}