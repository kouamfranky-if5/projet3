import * as utils from './module/utils.js'
import * as moduleMediaPhotographe from './module/collection-photographe.js'

// recuperation des infos sur le photographe courent
const photographe = JSON.parse(localStorage.getItem("photographe"));
const mediaPhotographe = JSON.parse(localStorage.getItem("mediaPhotographe"));
const srcDirMedia = photographe.name.split(" ")[0];
let totalLikes = mediaPhotographe.reduce((total, media) => total + media.likes, 0);

//console.log(photographe);
//console.log(mediaPhotographe);
let mediaPhotographeFilter = mediaPhotographe;

initDetail(photographe);




function initDetail(photographe) {

    // Entete de la page 
    let html = document.querySelector(".root");
    let createHeader = getHeader(photographe);
    let createFilter = createEltFiltre();
    let createRecapLikes = createRecapLikesPhotographe(photographe, mediaPhotographe);
    let createLightboxMedia = createLightbox();
    let createContacterMoi = createContactezMoi(photographe);
    let createCollectionPhotographe = createCollectionMedia(mediaPhotographe, photographe);


    html.appendChild(createHeader);
    html.appendChild(createFilter);
    html.appendChild(createCollectionPhotographe);
    html.appendChild(createRecapLikes);
    html.appendChild(createLightboxMedia);
    html.appendChild(createContacterMoi);

    onclickMedia(mediaPhotographe);

    onclickFiltre(mediaPhotographe);
    onClickLike(mediaPhotographe);
    onclickContactezMoi();

}

// construction de l'entete
function getHeader(photographe) {
    let content = document.createElement("header");
    content.innerHTML = `
    <div class="header_detail">
        <a href="index.html" ><img src="assets/images/logo/logo.png" alt="logo FishEye"></a>
        <div class="card_detail_photofgraphe">
            <div class="info_photofgraphe">
                <p class="info_photofgraphe--titre">${photographe.name}</p>
                <p class="info_photofgraphe--localisation"> Auckland, New Zealand </p>
                <p class="info_photofgraphe--description">${photographe.tagline}</p>
                ${utils.getTagsPhotographe(photographe.tags)}
            </div>
            <button id="open_contactez_moi" > Contactez-moi </button>
            <div class="card_img_profil">
                <a href="#">
                    <img src="assets/images/Photographers_ID_Photos/${photographe.portrait}"
                        alt="photo de ${photographe.name}">
                </a>
            </div>
        </div>
    </div> 
    `;
    //console.log(allTags);
    return content;
}

// construction du corps de la page
// creation de la section pour afficher la liste des photographes 
// create corps media
function createCollectionMedia(mediaPhotographe, photographe) {
    let collectionPhoto = document.createElement("div");
    collectionPhoto.classList.add("collection_photographe");
    return moduleMediaPhotographe.getCollectionPhotographe(mediaPhotographe, collectionPhoto, photographe.name.split(" ")[0]);
}

// contruction initial du calcul des likes
function createRecapLikesPhotographe(photographe, mediaPhotographe) {
    //console.log("totalLikes", totalLikes);
    let recapLikes = document.createElement("div");
    recapLikes.classList.add("recap-detail");
    recapLikes.innerHTML = ` 
    <span class="recap-likes">${totalLikes} <i class="fas fa-heart"> </i> </span>
    <span>${photographe.price}€/jour</span>
    `;
    return recapLikes;
}

// initialisation de la lightbox
function createLightbox() {
    let content = document.createElement("div");
    content.classList.add("hidden");
    content.id = "lightbox";
    content.innerHTML = ` 
    <div class="lightbox__content">
        <aside class="content__media"> </aside> 
        <button id="closeLightbox">&#10005;</button>
        <button class="prev">&#8249;</button>
        <button class="next">&#8250;</button>
    </div>
    `;
    return content;
}

// initialisation du formulaire de contact
function createContactezMoi(photographe) {
    let content = document.createElement("div");
    content.classList.add("hidden");
    content.id = "contactez_moi";
    content.innerHTML = `  
    <form class="contactez_moi_form" action="">
        <a id="close_contactez_moi">&times;</a> 
        <p>Contactez-moi <br> ${photographe.name} </p>

        <div class="form-group">
            <label for="prenom"> Prénom </label><br>
            <input type="text">
        </div>
        <div class="form-group">
            <label for="nom"> Nom </label><br>
            <input type="text">
        </div>
        <div class="form-group">
            <label for="email"> Email </label><br>
            <input type="email">
        </div>
        <div class="form-group">
            <label for="message"> Votre message </label><br> 
            <textarea name="message" id="" cols="30" rows="10"></textarea>
        </div>
        <button id="envoyer_contantez_moi" > Envoyer </button>
    </form>  
    `;
    return content;
}

// construction du filtre
function createEltFiltre() {
    let content = document.createElement("div");
    content.classList.add("trie");
    content.innerHTML = ` 
        <p><b>Trier par</b></p>
        <div class ='trie_elt' >
        <nav class="dropdown">
            <label for="touch"><span class="text_filter">Popularité</span> </label>
            <input type="checkbox" id="touch">
            <ul class="slide">
                <li><a class="filter1" href="#1">Date</a></li>
                <li><a class="filter2" href="#2">Titre</a></li>
            </ul>
        </nav>
        </div>
    `;
    //console.log(allTags);
    return content;
}


//Ecouter du click sur les elements de filtre 
function onclickFiltre(mediaPhotographe) {
    // mediaPhotographe.forEach(media => {

    let filterSelected = document.querySelector(".text_filter");
    let filter1 = document.querySelector(".filter1");
    let filter2 = document.querySelector(".filter2");
    filter1.addEventListener("click", function (event) {
        let tempElt = filterSelected.innerText;
        filterSelected.innerHTML = event.target.innerText;
        filter1.innerHTML = tempElt;

        console.log(event, mediaPhotographe);
        onFilter(filterSelected, mediaPhotographe);
    });
    filter2.addEventListener("click", function (event) {
        let tempElt = filterSelected.innerText;
        filterSelected.innerHTML = event.target.innerText;
        filter2.innerHTML = tempElt;

        onFilter(filterSelected, mediaPhotographe);
    });

    function onFilter(event, mediaPhotographe) {
        if (event.innerText == "Popularité") {
            mediaPhotographeFilter = mediaPhotographe.sort((media1, media2) => (media1.likes < media2.likes) ? 1 : -1);
            console.log("Popularité", mediaPhotographeFilter);
        }
        if (event.innerText == "Date") {
            mediaPhotographeFilter = mediaPhotographe.sort((media1, media2) =>
                (Date.parse(media1.date) < Date.parse(media2.date)) ? 1 : -1);

            console.log("Date", mediaPhotographeFilter);
        }
        if (event.innerText == "Titre") {
            mediaPhotographeFilter = mediaPhotographe.sort((media1, media2) =>
                (media1.title.toUpperCase() > media2.title.toUpperCase()) ? 1 : -1);

            console.log("Titre", mediaPhotographeFilter);
        }

        console.log(mediaPhotographeFilter);
        moduleMediaPhotographe.getCollectionPhotographe(mediaPhotographeFilter, document.querySelector(".collection_photographe"), photographe.name.split(" ")[0]);
        onclickMedia(mediaPhotographeFilter);
        onClickLike(mediaPhotographe);
    }

}


// mise a jours du nombre de like apres un like ou dislike
function onClickLike(mediasPh) {

    const recapLikes = document.querySelector(".recap-likes");
    mediasPh.forEach(media => {
        let curentMedia = document.getElementById("heart_" + media.id);
        curentMedia.addEventListener("click", function (event) {
            // console.log(recapLikes.textContent);
            // console.log(curentMedia.textContent);
            let elt = curentMedia.getElementsByTagName("i")[0];
            if (elt.classList.contains("fas")) {
                curentMedia.innerHTML = `${parseInt(curentMedia.textContent) - 1} <i class="far fa-heart"> </i>`;
                recapLikes.innerHTML = `${parseInt(recapLikes.textContent) - 1} <i class="fas fa-heart"> </i>`;
            } else {
                curentMedia.innerHTML = `${parseInt(curentMedia.textContent) + 1} <i class="fas fa-heart"> </i>`;
                recapLikes.innerHTML = `${parseInt(recapLikes.textContent) + 1} <i class="fas fa-heart"> </i>`;
            }
        });
    });
}

// ecoute de l'evenement de click sur le bouton contactez-moi
function onclickContactezMoi() {
    const closeBtn = document.getElementById("close_contactez_moi");
    const sendBtn = document.getElementById("envoyer_contantez_moi");
    const openBtn = document.getElementById("open_contactez_moi");
    const contactezMoi = document.getElementById("contactez_moi");


    function closeContacterMoi(event) {
        contactezMoi.classList.add("hidden");
        // console.log(event);
    }


    function openContacterMoi(event) {
        //console.log(event);
        // on retire la propriete hidden (css a display:none) pour pouvoir afficher le formulaire
        contactezMoi.classList.remove("hidden");
    }

    function sendContacterMoi(event) {
        event.preventDefault();
        console.log(event);
    }


    closeBtn.addEventListener("click", (event) => closeContacterMoi(event));
    openBtn.addEventListener("click", (event) => openContacterMoi(event));
    sendBtn.addEventListener("click", (event) => sendContacterMoi(event));
}

//Ecouter du click pour la page de detail
function onclickMedia(mediaPhotographe) {
    // console.log("mediaPhotographe", mediaPhotographe);
    const closeBtn = document.getElementById("closeLightbox");
    const prevBtn = document.querySelector(".prev");
    const nextBtn = document.querySelector(".next");
    const lightbox = document.getElementById("lightbox");
    const content = document.querySelector(".content__media");
    const mediasPh = mediaPhotographe;

    // console.log(mediasPh);

    mediasPh.forEach(photographe => {
        let curentMedia = document.getElementById("media_" + photographe.id);
        curentMedia.addEventListener("click", function (event) {
            //console.log(event.target);
            openLightbox(photographe.id);
        });
    });


    //console.log(mediasPh);
    let currentImage = 0;

    function closeLightbox() {
        lightbox.classList.add("hidden");
    }

    function prevImage() {
        currentImage--;
        if (currentImage < 0) currentImage = mediasPh.length - 1;
        content.innerHTML = getContentLightbox(mediasPh[currentImage]);
    }

    function nextImage() {
        currentImage++;
        if (currentImage >= mediasPh.length) currentImage = 0;
        content.innerHTML = getContentLightbox(mediasPh[currentImage]);
    }

    function getContentLightbox(item) {
        return (item.image) ?
            `<article class="content__card_img"><a><img  src="assets/images/${srcDirMedia}/${item.image}" alt="photo de ${item.title}"></a></article>`
            : `<article class="content__card_video"><video controls src="assets/images/${srcDirMedia}/${item.video}"> video de ${item.title} </video></article>`;
    }

    function openLightbox(idMedia) {
        // on retire la propriete hidden (css a display:none) pour pouvoir affichier la lightbox
        lightbox.classList.remove("hidden");
        // on place limage courant avec l'index de lelement selectionne
        currentImage = mediasPh.findIndex(item => item.id == idMedia);
        content.innerHTML = getContentLightbox(mediasPh.filter(item => item.id == idMedia)[0]);
    }


    closeBtn.addEventListener("click", closeLightbox);
    prevBtn.addEventListener("click", prevImage);
    nextBtn.addEventListener("click", nextImage);

}


