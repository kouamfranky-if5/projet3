import * as utils from './module/utils.js'
import * as moduleProfilPhotographe from './module/profil-photographe.js'


// Récupération des pièces depuis le fichier JSON
// const tempData = await fetch("assets/data/FishEyeData.json");
// const reponses = await tempData.json();
const reponses = await fetch("assets/data/FishEyeData.json").then(photographe => photographe.json());
const dataPhotographe = reponses.photographers
const mediaPhotographe = reponses.media
// variable globale
let filtreSelectionner = [];
let allTagsPhotographes = utils.getAllTags(dataPhotographe);
localStorage.removeItem("mediaPhotographe");

console.log(dataPhotographe);

// Initialisation de la page d'accueil
initIndex();


// Construction de la page d'accueil
function initIndex() {

    // Entete de la page 
    let html = document.querySelector(".root");
    let header = getHeader(allTagsPhotographes);
    // construction du corps de la page
    // creation de la section pour afficher la liste des photographes
    let photographes = document.createElement("div");
    photographes.classList.add("photographes");
    let profilPhotographe = moduleProfilPhotographe.getProfifPhotographe(dataPhotographe, photographes);

    html.appendChild(header);
    html.appendChild(profilPhotographe);
    onclickFiltre();
    onclickPhotographe(dataPhotographe);
}


// construction de l'entete
function getHeader(allTags) {
    let content = document.createElement("header");
    content.innerHTML = `<img src="assets/images/logo/logo.png" alt="logo FishEye">
    <div class="filtre_tag_name">
    ${utils.getTagsPhotographe(allTags)}  
    </div>
    <h1>Nos photographes</h1>`;
    console.log(allTags);
    return content;
}


//Ecouter du click sur les elements de filtre 
function onclickFiltre() {
    allTagsPhotographes.forEach(tag => {

        let tagSelect = document.querySelector(".tag_" + tag);
        tagSelect.addEventListener("click", function (event) {
            // ajouter ou retirer un tag de filtre
            if (!filtreSelectionner.includes(event.target.innerHTML)) {
                filtreSelectionner.push(event.target.innerHTML);
                tagSelect.classList.add("tag_name_select");
            } else {
                filtreSelectionner.splice(filtreSelectionner.indexOf(event.target.innerHTML), 1);
                tagSelect.classList.remove("tag_name_select");
            }

            // // filtre la liste des photographes 
            let photographeFiltrees = dataPhotographe.filter(function (photographe) {
                return utils.contentArray(photographe.tags, filtreSelectionner);
            });
            console.log(filtreSelectionner);
            console.log(photographeFiltrees);
            // mise a jours du corps de la page
            moduleProfilPhotographe.getProfifPhotographe(photographeFiltrees, document.querySelector(".photographes"));
            onclickPhotographe(photographeFiltrees);
        });

    });
}

//Ecouter du click pour la page de detail
function onclickPhotographe(dataPhotographes) {
    dataPhotographes.forEach(photographe => {
        let curentPhoto = document.getElementById(photographe.id);
        curentPhoto.addEventListener("click", function (event) {
            console.log(event.target);
            localStorage.setItem("photographe", JSON.stringify(photographe));
            // console.log(mediaPhotographe.filter(media=> media.photographerId==photographe.id));
            localStorage.setItem("mediaPhotographe", JSON.stringify(mediaPhotographe.filter(media => media.photographerId == photographe.id)));
        });
    });
}








